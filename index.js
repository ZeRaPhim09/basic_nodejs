const Truk = require("./truk");

function main() {
  let truck = new Truk("Hino", "AG 5644 CD", "Fuso", 1000000, 1500);
  console.log("Halo ini truk", truck.nama, " dengan plat nomor", truck.plat);
  console.log("Truk ini berjenis", truck.jenisTruk, " dengan maksimal muatan", truck.maxMuatan, "kg");
  console.log("Tarif untuk menyewa truk ", truck.nama, " ialah Rp.", truck.tarif);

  truck.drive(30);
  truck.stop("Jakarta");
  truck.time(10);
}

main();

class Truk {
  constructor(nama, plat, jenisTruk, tarif, maxMuatan) {
    this.nama = nama;
    this.plat = plat;
    this.jenisTruk = jenisTruk;
    this.tarif = tarif;
    this.maxMuatan = maxMuatan;
  }
  drive(kecepatan) {
    console.log("Kecepatan truk yaitu", kecepatan, "Km/jam");
  }
  stop(tujuan) {
    console.log("Tujuan akhir pengiriman di " + tujuan);
  }
  time(waktuPengiriman) {
    console.log("Waktu untuk pengiriman yaitu " + waktuPengiriman + " hari");
  }
}
module.exports = Truk;
